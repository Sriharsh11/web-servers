const express = require('express');
const hbs = require('hbs');
var app = express();
hbs.registerPartials(__dirname + '/views/partials');
app.set('view engine','hbs');
app.get('/',(req,res) => {
  res.render('home.hbs',{
    name : 'Sriharsh',
    year : new Date().getFullYear()
  });
});
app.get('/bad',(req,res) => {
  res.send({
    ErrorMessage : 'error handler'
  });
});
app.get('/about',(req,res) => {
  res.render('about.hbs',{
    name : 'Sriharsh'
  });
});
app.listen(3000,() => {
  console.log('The server is up');
});
